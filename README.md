# BG Status Bar

**No longer under development**, if you are looking for a good tool to do this job, [Sugarmate](https://sugarmate.io) is a great commercial option, and there are a few other OSS options as well (<https://github.com/mddub/nightscout-osx-menubar>, <https://github.com/mpangburn/NightscoutMenuBar>).

---

Pet project for reading CGM data from a source (Nightscout, Dexcom API, etc) and putting a number and a trend arrow in your Mac's status bar. And let's not forget about notifications, we have got those too!

NOT intended for use in any medical decision making capacity.

#TODO
* if data is older than X, should put a ??? in status bar instead of an old number
* tests
* change name to bg MENU bar
* Network connection status
* follow-up alerts as an action on notifications...
    * like, "alert me again if trend is still down in x minutes" something like that
* restore settings and continue work on them
    * type enforcement in settings bindings
        * continue with this just using control types - but this requires dealing with...
        * preference for units
            * implement usage of valuetransformer in settings
                * if mg/dl is selected, remove transformer, if mmol/L is selected add transformer
* animate trend arrow change
- fall rate alert not highlighting status item OR latest entry item
- status bar item text highlighting on stale data doesn't seem to be working
* for 0.2.2 release, restore settings but remove unit choice
* for 0.3 release, finish unit switching

x for 0.2.1 release, colour scheme for notification colours, update font usage everywhere we use attributed strings to be more in line with plain system font usage in non-attributed spots
x NEED TO INDICATE GAPS IN DATA, like in this sample... (or just put the x minutes ago counts into the menu items)
```
[{"_id":"5a201a1cff863e866b13a498","sgv":104,"date":1512053171000,"dateString":"2017-11-30T14:46:11.000Z","trend":2,"direction":"SingleUp","device":"share2","type":"sgv"},{"_id":"5a2018f0ff863e866b13940b","sgv":100,"date":1512052870000,"dateString":"2017-11-30T14:41:10.000Z","trend":2,"direction":"SingleUp","device":"share2","type":"sgv"},{"_id":"5a2017c3ff863e866b13875d","sgv":88,"date":1512052570000,"dateString":"2017-11-30T14:36:10.000Z","trend":3,"direction":"FortyFiveUp","device":"share2","type":"sgv"},{"_id":"5a201697ff863e866b137c9d","sgv":74,"date":1512052270000,"dateString":"2017-11-30T14:31:10.000Z","trend":4,"direction":"Flat","device":"share2","type":"sgv"},{"_id":"5a20156bff863e866b136fcd","sgv":62,"date":1512051971000,"dateString":"2017-11-30T14:26:11.000Z","trend":4,"direction":"Flat","device":"share2","type":"sgv"},{"_id":"5a20143fff863e866b136aaa","sgv":61,"date":1512051670000,"dateString":"2017-11-30T14:21:10.000Z","trend":4,"direction":"Flat","device":"share2","type":"sgv"},{"_id":"5a201313ff863e866b135b00","sgv":60,"date":1512051371000,"dateString":"2017-11-30T14:16:11.000Z","trend":4,"direction":"Flat","device":"share2","type":"sgv"},{"_id":"5a2011e7ff863e866b1355c6","sgv":60,"date":1512051075000,"dateString":"2017-11-30T14:11:15.000Z","trend":5,"direction":"FortyFiveDown","device":"share2","type":"sgv"},{"_id":"5a1ff6f3ff863e866b12c909","sgv":125,"date":1512043570000,"dateString":"2017-11-30T12:06:10.000Z","trend":5,"direction":"FortyFiveDown","device":"share2","type":"sgv"},{"_id":"5a1ff36fff863e866b12bdf4","sgv":133,"date":1512043270000,"dateString":"2017-11-30T12:01:10.000Z","trend":4,"direction":"Flat","device":"share2","type":"sgv"}]
```