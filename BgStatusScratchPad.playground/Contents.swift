//: Playground - noun: a place where people can play

import Foundation


import Foundation

let dexcomAPIDateFormatter = DateFormatter()
dexcomAPIDateFormatter.locale = Locale(identifier: "en_US_POSIX")// should be derived from local system, right?
dexcomAPIDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"// iso8601 but without the Z
dexcomAPIDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)


struct DexcomAPIEGVResponse: Codable {
	let unit: String
	let rateUnit: String
	let egvs: [DexcomAPIEGV]
}

struct DexcomAPIEGV: Codable {
	let systemTime: Date// GMT
//	let displayTime: Date
	let value: Int
	let status: String?
	let trend: DexcomEGVTrend
	let trendRate: Double
}

enum DexcomEGVTrend: String, Codable {
	case doubleUp, singleUp, fortyFiveUp, flat, fortyFiveDown, singleDown, doubleDown, none, notComputable, rateOutOfRange
}

let egvSample = """
{
	"unit": "mg/dL",
	"rateUnit": "mg/dL/min",
	"egvs": [
	{
	"systemTime": "2017-06-16T15:40:00",
	"displayTime": "2017-06-16T07:40:00",
	"value": 119,
	"status": null,
	"trend": "fortyFiveDown",
	"trendRate": -1.3
	},
	{
	"systemTime": "2017-06-16T15:35:00",
	"displayTime": "2017-06-16T07:35:00",
	"value": 126,
	"status": null,
	"trend": "fortyFiveDown",
	"trendRate": -1.2
	},
	{
	"systemTime": "2017-06-16T15:30:00",
	"displayTime": "2017-06-16T07:30:00",
	"value": 132,
	"status": null,
	"trend": "fortyFiveDown",
	"trendRate": -1.1
	}
	]
}
"""

let liveEgvSample = "{\"unit\":\"mg/dL\",\"rateUnit\":\"mg/dL/min\",\"egvs\":[{\"systemTime\":\"2017-09-18T23:55:00\",\"displayTime\":\"2017-09-18T15:55:00\",\"value\":139,\"status\":null,\"trend\":\"flat\",\"trendRate\":0},{\"systemTime\":\"2017-09-18T23:50:00\",\"displayTime\":\"2017-09-18T15:50:00\",\"value\":138,\"status\":null,\"trend\":\"flat\",\"trendRate\":-0.1},{\"systemTime\":\"2017-09-18T23:45:00\",\"displayTime\":\"2017-09-18T15:45:00\",\"value\":139,\"status\":null,\"trend\":\"flat\",\"trendRate\":0},{\"systemTime\":\"2017-09-18T23:40:00\",\"displayTime\":\"2017-09-18T15:40:00\",\"value\":139,\"status\":null,\"trend\":\"flat\",\"trendRate\":-0.1},{\"systemTime\":\"2017-09-18T23:35:00\",\"displayTime\":\"2017-09-18T15:35:00\",\"value\":139,\"status\":null,\"trend\":\"flat\",\"trendRate\":0},{\"systemTime\":\"2017-09-18T23:30:00\",\"displayTime\":\"2017-09-18T15:30:00\",\"value\":140,\"status\":null,\"trend\":\"flat\",\"trendRate\":0.1},{\"systemTime\":\"2017-09-18T23:25:00\",\"displayTime\":\"2017-09-18T15:25:00\",\"value\":139,\"status\":null,\"trend\":\"flat\",\"trendRate\":-0.1},{\"systemTime\":\"2017-09-18T23:20:00\",\"displayTime\":\"2017-09-18T15:20:00\",\"value\":139,\"status\":null,\"trend\":\"flat\",\"trendRate\":-0.3},{\"systemTime\":\"2017-09-18T23:15:00\",\"displayTime\":\"2017-09-18T15:15:00\",\"value\":140,\"status\":null,\"trend\":\"flat\",\"trendRate\":-0.4},{\"systemTime\":\"2017-09-18T23:10:00\",\"displayTime\":\"2017-09-18T15:10:00\",\"value\":142,\"status\":null,\"trend\":\"flat\",\"trendRate\":-0.3},{\"systemTime\":\"2017-09-18T23:05:00\",\"displayTime\":\"2017-09-18T15:05:00\",\"value\":144,\"status\":null,\"trend\":\"flat\",\"trendRate\":-0.1},{\"systemTime\":\"2017-09-18T23:00:00\",\"displayTime\":\"2017-09-18T15:00:00\",\"value\":145,\"status\":null,\"trend\":\"flat\",\"trendRate\":0}]}"

let sampleData = egvSample.data(using: .utf8)!

let decoder = JSONDecoder()
decoder.dateDecodingStrategy = .formatted(dexcomAPIDateFormatter)// no good for display time, this will be wrong time zone
let decodedEgvs = try! decoder.decode(DexcomAPIEGVResponse.self, from: sampleData)

decodedEgvs.egvs[0]


