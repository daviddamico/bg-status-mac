//
//  bg_status_macTests.swift
//  bg-status-macTests
//
//  Created by David D'Amico on 2017-08-29.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import XCTest
@testable import bg_status_mac

class nightscout_status_macTests: XCTestCase {
	
	override func setUp() {
		super.setUp()
		// Put setup code here. This method is called before the invocation of each test method in the class.
	}
	
	override func tearDown() {
		// Put teardown code here. This method is called after the invocation of each test method in the class.
		super.tearDown()
	}
	
	func testNightscoutEntryInstantiation() {
		
		let now = Date()
		let nightscoutAPIDateFormatter = DateFormatter()
		nightscoutAPIDateFormatter.locale = Locale(identifier: "en_US_POSIX")// should be derived from local system, right?
		nightscoutAPIDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
		nightscoutAPIDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
		
		// @TODO change this to instantiate from a real json sample
		let sampleJson = """
{"_id":"5a1ad304ff863e866bfe2a31","sgv":157,"date":1511707282000,"dateString":"2017-11-26T14:41:22.000Z","trend":4,"direction":"Flat","device":"share2","type":"sgv"}
"""
		let decoder = JSONDecoder()
		do {
			let sampleEntry = try decoder.decode(NightscoutEntry.self, from: sampleJson.data(using: .utf8)!)
			XCTAssert(sampleEntry.type == "sgv")
		} catch {
			print("error trying to convert data to JSON")
			print(error)
		}
	}
	
	func testNightscoutTrendMappings() {
		
		let now = Date()
		let nightscoutAPIDateFormatter = DateFormatter()
		nightscoutAPIDateFormatter.locale = Locale(identifier: "en_US_POSIX")// should be derived from local system, right?
		nightscoutAPIDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
		nightscoutAPIDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)

		let noneNsEntry = NightscoutEntry(id: "1", sgv: 80, date: now.timeIntervalSince1970, dateString: nightscoutAPIDateFormatter.string(from: now), trend: NightscoutTrend(rawValue: 0)!, direction: "NONE", type: "sgv")
		let doubleUpNsEntry = NightscoutEntry(id: "2", sgv: 80, date: now.timeIntervalSince1970, dateString: nightscoutAPIDateFormatter.string(from: now), trend: NightscoutTrend(rawValue: 1)!, direction: "DoubleUp", type: "sgv")
		let singleUpNsEntry = NightscoutEntry(id: "3", sgv: 80, date: now.timeIntervalSince1970, dateString: nightscoutAPIDateFormatter.string(from: now), trend: NightscoutTrend(rawValue: 2)!, direction: "SingleUp", type: "sgv")
		let fortyFiveUpNsEntry = NightscoutEntry(id: "4", sgv: 80, date: now.timeIntervalSince1970, dateString: nightscoutAPIDateFormatter.string(from: now), trend: NightscoutTrend(rawValue: 3)!, direction: "FortyFiveUp", type: "sgv")
		let flatNsEntry = NightscoutEntry(id: "5", sgv: 80, date: now.timeIntervalSince1970, dateString: nightscoutAPIDateFormatter.string(from: now), trend: NightscoutTrend(rawValue: 4)!, direction: "Flat", type: "sgv")
		let fortyFiveDownNsEntry = NightscoutEntry(id: "6", sgv: 80, date: now.timeIntervalSince1970, dateString: nightscoutAPIDateFormatter.string(from: now), trend: NightscoutTrend(rawValue: 5)!, direction: "FortyFiveDown", type: "sgv")
		let singleDownNsEntry = NightscoutEntry(id: "7", sgv: 80, date: now.timeIntervalSince1970, dateString: nightscoutAPIDateFormatter.string(from: now), trend: NightscoutTrend(rawValue: 6)!, direction: "SingleDown", type: "sgv")
		let doubleDownNsEntry = NightscoutEntry(id: "8", sgv: 80, date: now.timeIntervalSince1970, dateString: nightscoutAPIDateFormatter.string(from: now), trend: NightscoutTrend(rawValue: 7)!, direction: "DoubleDown", type: "sgv")

		let noneEntry = BGEntry(with: noneNsEntry)
		let doubleUpEntry = BGEntry(with: doubleUpNsEntry)
		let singleUpEntry = BGEntry(with: singleUpNsEntry)
		let fortyFiveUpEntry = BGEntry(with: fortyFiveUpNsEntry)
		let flatEntry = BGEntry(with: flatNsEntry)
		let fortyFiveDownEntry = BGEntry(with: fortyFiveDownNsEntry)
		let singleDownEntry = BGEntry(with: singleDownNsEntry)
		let doubleDownEntry = BGEntry(with: doubleDownNsEntry)

		XCTAssert(noneEntry.trend == .none)
		XCTAssert(doubleUpEntry.trend == .doubleUp)
		XCTAssert(singleUpEntry.trend == .singleUp)
		XCTAssert(fortyFiveUpEntry.trend == .fortyFiveUp)
		XCTAssert(flatEntry.trend == .flat)
		XCTAssert(fortyFiveDownEntry.trend == .fortyFiveDown)
		XCTAssert(singleDownEntry.trend == .singleDown)
		XCTAssert(doubleDownEntry.trend == .doubleDown)
	}
	
	func testPerformanceExample() {
		// This is an example of a performance test case.
		self.measure {
			// Put the code you want to measure the time of here.
		}
	}
	
}
