//
//  NightscoutAPIClient.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-08-29.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa

// @TODO switch to a singleton?
class NightscoutAPIClient: NSObject {

	let urlStem: URL
	let entriesEndpoint = "entries.json"
	
	init(endpointUrl: URL) {
		self.urlStem = endpointUrl.appendingPathComponent("api/v1")
		super.init()
	}
	
	func getLatestEntries (completionHandler: @escaping (NightscoutAPIEntriesResponse?, Error?) -> Void)
	{
		// request from stem/entries.json
		let configuration = URLSessionConfiguration.default
		let session = URLSession(configuration: configuration)
		
		let entriesUrl = urlStem.appendingPathComponent(entriesEndpoint)
		let urlTask = session.dataTask(with: entriesUrl, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
			
			if let responseData = data {
				let decoder = JSONDecoder()
				do {
					let incomingEntries = try decoder.decode([NightscoutEntry].self, from: responseData)
					let entries = NightscoutAPIEntriesResponse(entries: incomingEntries)// @TODO do we really want to do this? not really the response then
					completionHandler(entries, nil)
				} catch {
					print("error trying to convert data to JSON")
					print(error)
					completionHandler(nil, error)
				}
			}
		})
		urlTask.resume()
		
	}
	
}

struct NightscoutAPIEntriesResponse {
	let entries: [NightscoutEntry]
}

struct NightscoutEntry: Codable {
	var id: String
	var sgv: Int
	var date: Double
	var dateString: String
	var trend: NightscoutTrend
	var direction: String
	var type: String
	
	enum CodingKeys: String, CodingKey {
		case id = "_id"
		case sgv
		case date
		case dateString
		case trend
		case direction
		case type
	}
	
}

enum NightscoutTrend: Int, Codable {
	case NONE = 0
	case doubleUp = 1
	case singleUp = 2
	case fortyFiveUp = 3
	case flat = 4
	case fortyFiveDown = 5
	case singleDown = 6
	case doubleDown = 7
	
	var bgTrend: BGTrend? {
		switch self {
		case .flat: return .flat
		case .fortyFiveDown: return .fortyFiveDown
		case .fortyFiveUp: return .fortyFiveUp
		case .singleUp: return .singleUp
		case .doubleUp: return .doubleUp
		case .singleDown: return .singleDown
		case .doubleDown: return .doubleDown
		case .NONE: return BGTrend.none
		default: return BGTrend.none
		}
	}
}
