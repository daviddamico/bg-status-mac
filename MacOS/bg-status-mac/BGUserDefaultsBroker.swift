//
//  BGUserDefaultsBroker.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-09-04.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa

class BGUserDefaultsBroker {
	
	let defaults: UserDefaults = UserDefaults.standard
	
	// @TODO implement a value getter here... this is an okay
	// change, but we kind of need to validate the values on
	// the way IN, otherwise we can get into a bad state pretty
	// quickly
}
