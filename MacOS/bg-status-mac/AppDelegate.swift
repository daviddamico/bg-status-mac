//
//  AppDelegate.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-08-29.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa

@NSApplicationMain
class AppDelegate: NSObject, NSApplicationDelegate, NSUserNotificationCenterDelegate {
	
	var statusItem = NSStatusBar.system.statusItem(withLength: NSStatusItem.variableLength)
	var menuCoordinator: BGMenuCoordinator?
	var BGCoordinator: BGStatusCoordinator?
	var settingsWindowController: NSWindowController?
	
	func applicationDidFinishLaunching(_ aNotification: Notification) {
		
		let userNotificationCenter = NSUserNotificationCenter.default
		userNotificationCenter.delegate = self
		
		// okay but how to enforce these types in the bindings?
		if let bundle = Bundle.main.bundleIdentifier {
			UserDefaults.standard.removePersistentDomain(forName: bundle)
		}// @TODO remove, for debugging
		UserDefaults.standard.register(defaults: [
			"nightscoutInstanceURL": "https://ddamicons.azurewebsites.net/",
			"lowThreshold": 72.0,// 4.0,
			"urgentLowThreshold": 54.0,// 3.0,
			"highThreshold": 216.0,// 12.0,
			"entryBestBeforeInMinutes": 15,
			"openAtLogin": false,
			// @TODO change to display units or something like that
			"bgUnits": BGUnitPreferences.mmolL.rawValue//"mmol/L"// or mg/dL
		])
		
		let nightscoutURL = URL(string: UserDefaults.standard.string(forKey: "nightscoutInstanceURL")!)!
		self.statusItem.menu = NSMenu()
		if let menu = self.statusItem.menu {
			self.menuCoordinator = BGMenuCoordinator(with: menu)
		}
		
		if let statusButton = self.statusItem.button {
			statusButton.title = "updating"
		}
		
		
		
		// @TODO SO! instead of all this, we need to check if our settings contain
		// an instance URL. If they don't, present the settings window and get
		// the coordinator started when one is saved. If they do, then proceed.
		if let instanceURL = UserDefaults.standard.string(forKey: "nightscoutInstanceURL") {
			if instanceURL != "" {
				self.BGCoordinator = BGStatusCoordinator(with: nightscoutURL)
				self.BGCoordinator!.startUpdating()// @TODO don't force unwrap
			} else {
				self.openSettingsWindow()
			}
		}
		
		
		UserDefaults.standard.addObserver(self, forKeyPath: "nightscoutInstanceURL", options: .new, context: nil)
		UserDefaults.standard.addObserver(self, forKeyPath: "lowThreshold", options: .new, context: nil)
		UserDefaults.standard.addObserver(self, forKeyPath: "urgentLowThreshold", options: .new, context: nil)
		UserDefaults.standard.addObserver(self, forKeyPath: "highThreshold", options: .new, context: nil)
		UserDefaults.standard.addObserver(self, forKeyPath: "openAtLogin", options: .new, context: nil)
		
	}
	
	override func observeValue(forKeyPath keyPath: String?, of object: Any?, change: [NSKeyValueChangeKey : Any]?, context: UnsafeMutableRawPointer?) {
		if (keyPath == "lowThreshold" || keyPath == "urgentLowThreshold" || keyPath == "highThreshold") {
			
		} else if (keyPath == "nightscoutInstanceURL") {
			
		} else if (keyPath == "openAtLogin") {
			
		}
		print("here")
	}

	func applicationWillTerminate(_ aNotification: Notification) {
		// Insert code here to tear down your application
		self.BGCoordinator?.stopUpdating()
	}
	
	func userNotificationCenter(_ center: NSUserNotificationCenter, shouldPresent notification: NSUserNotification) -> Bool {
		return true
	}
	
	func application(_: NSApplication, open: [URL]) {
		let url = open[0]
		if (url.host == "oauth-callback") {
			DexcomAPIClient.shared.processOAuthStep1Response(url: url)
		}
	}
	
	@objc func didClickSettingsItem (_ sender: Any?) {
		// check if already open, otherwise we can get dupes
		
		if (self.settingsWindowController == nil) {
			self.openSettingsWindow()
		}
		
		NSApplication.shared.activate(ignoringOtherApps: true)
		
	}
	
	func openSettingsWindow () {
		let sb = NSStoryboard(name: NSStoryboard.Name("Main"), bundle: nil)
		self.settingsWindowController = sb.instantiateController(withIdentifier: NSStoryboard.SceneIdentifier("SettingsWindowController")) as? NSWindowController
		
		NotificationCenter.default.addObserver(self, selector: #selector(self.settingsWindowWillClose), name: NSWindow.willCloseNotification, object: self.settingsWindowController?.window)
		
		self.settingsWindowController?.showWindow(self)
	}
	
	@objc func settingsWindowWillClose (_ sender: Any?) {
		self.settingsWindowController = nil
		NotificationCenter.default.removeObserver(self)
	}
	
	@objc func quitApplication (_ sender: Any?) {
		NSApplication.shared.terminate(self)
	}
}

// @TODO not sure where this goes, but it's not here
// @TODO add a default case?
enum BGUnitPreferences: String {
	case mmolL = "mmol/L"
	case mgdl = "mg/dl"
}

