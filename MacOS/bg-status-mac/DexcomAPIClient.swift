//
//  DexcomAPIClient.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-09-11.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa

protocol OAuth2Authenticable {
	// @TODO move DexcomAPIClient implementations into default implementations
	func hasOauthToken () -> Bool
	func startOAuth2Login () -> Void
	func processOAuthStep1Response (url: URL) -> Void
}

protocol OAuth2LoginURL {
	
}

class DexcomAPIClient: NSObject, OAuth2Authenticable {
	
	static let shared = DexcomAPIClient()
	
	let useSandbox: Bool = true
	let prodUrlStem = URL(string: "https://api.dexcom.com")!
	let sandboxUrlStem = URL(string: "https://sandbox-api.dexcom.com")!
	
	let authComponent = "/v1/oauth2/login"
	let tokenComponent = "/v1/oauth2/token"
	let egvsComponent = "/v1/users/self/egvs"
	
//	let dexcomAPIDateFormatter = DateFormatter()
//	dexcomAPIDateFormatter.locale = Locale(identifier: "en_US_POSIX")// should be derived from local system, right?
//	dexcomAPIDateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
//	dexcomAPIDateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
	
	// @TODO these get passed in
	let clientId = "NV7U5aaaJRvWe5uMOOg2SphBXaMA2s4Z"
	let clientSecret = "KIG9JqZ3uzjeBTJn"
	let appRedirectUri = "bgstatusbar://oauth-callback"// @TODO this gets passed in as well, and the events on appdelegate should get delegated to us
	
//	let clientId: String
//	let clientSecret: String
	
	let scope = "offline_access"
	let responseType = "code"
	
	var credentials: DexcomAPICredentials?
	
	var OAuthCompletionHandler: ((Error?) -> Void)?
	
	
	func hasOauthToken () -> Bool {
		return !(self.credentials == nil)
	}
	
	func startOAuth2Login () -> Void {
		// https://grokswift.com/alamofire-OAuth2/
		
		let stem = (self.useSandbox) ? self.sandboxUrlStem : self.prodUrlStem
		var urlComponents = URLComponents(string: stem.absoluteString)!
		urlComponents.path = self.authComponent
		urlComponents.query = "client_id=\(self.clientId)&redirect_uri=\(self.appRedirectUri)&response_type=code&scope=offline_access&state=TEST_STATE"
		let authUrlWithQuery = urlComponents.url!

		NSWorkspace.shared.open(authUrlWithQuery)
	}
	
	func processOAuthStep1Response (url: URL) -> Void {
		
		var code: String?
		
		let urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: false)!
		if let queryParameters = urlComponents.queryItems {
			for param in queryParameters {
				if (param.name.lowercased() == "code") {
					code = param.value
					break
				}
			}
		}
		
		if let receivedCode = code {
			self.didReceiveOAuth2Code(code: receivedCode)
		}
	}
	
	
	
	func didReceiveOAuth2Code (code: String) -> Void {
		let stem = (self.useSandbox) ? self.sandboxUrlStem : self.prodUrlStem
		
		var urlComponents = URLComponents(string: stem.absoluteString)!
		urlComponents.path = self.tokenComponent
		let tokenUrl = urlComponents.url!
		
		let headers = [
			"content-type": "application/x-www-form-urlencoded",
			"cache-control": "no-cache"
		]
		
		// why does this not work when created as a single string? learn more about String -> Data
		let postData = NSMutableData(data: "client_secret=\(self.clientSecret)".data(using: String.Encoding.utf8)!)
		postData.append("&client_id=\(self.clientId)".data(using: String.Encoding.utf8)!)
		postData.append("&code=\(code)".data(using: String.Encoding.utf8)!)
		postData.append("&grant_type=authorization_code".data(using: String.Encoding.utf8)!)
		postData.append("&redirect_uri=\(self.appRedirectUri)".data(using: String.Encoding.utf8)!)
		
		var request = URLRequest(url: tokenUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
		request.httpMethod = "POST"
		request.allHTTPHeaderFields = headers
		request.httpBody = postData as Data
		
		// @TODO then send request
		let session = URLSession.shared
		let task = session.dataTask(with: request, completionHandler: { (data, response, error) -> Void in
			
			if (error != nil) {
				print(error?.localizedDescription)
			} else {
				if let httpResponse = response {
					// @TODO add an error type here for oauth failure
//					print(httpResponse)
					if let jsonData = data {
						do {
							let jsonDecoder = JSONDecoder()
							let oauthResponse = try jsonDecoder.decode(DexcomAPITokenResponse.self, from: jsonData)
							self.credentials = DexcomAPICredentials(access_token: oauthResponse.access_token, refresh_token: oauthResponse.refresh_token)
							
							if let completionHandler = self.OAuthCompletionHandler {
								completionHandler(nil)
							}
							print("did we get it?")
						} catch {
							print("What happened: \(error)")
//							self.credentials = nil
						}
					}
				}
			}
		})
		task.resume()
	}
	
	func getEGVs (startDate: Date, endDate: Date) {
		
		if let credentials = self.credentials {
			let headers = [
				"authorization": "Bearer \(credentials.access_token)"
			]
			
			let stem = (self.useSandbox) ? self.sandboxUrlStem : self.prodUrlStem
			var urlComponents = URLComponents(string: stem.absoluteString)!
			urlComponents.path = self.egvsComponent
			
			let dateFormatter = DexcomAPIDateFormatter()
//			let dateFormatter = ISO8601DateFormatter()// can provoke error response with this format
			urlComponents.queryItems = [
				URLQueryItem(name: "startDate", value: dateFormatter.string(from: startDate)),
				URLQueryItem(name: "endDate", value: dateFormatter.string(from: endDate))
			]
			let egvsUrl = urlComponents.url!
			
			let request = NSMutableURLRequest(url: egvsUrl, cachePolicy: .useProtocolCachePolicy, timeoutInterval: 10.0)
			request.httpMethod = "GET"
			request.allHTTPHeaderFields = headers
			
			let session = URLSession.shared
			let task = session.dataTask(with: request as URLRequest, completionHandler: { (data, response, error) -> Void in
				if (error != nil) {
					print(error)
				} else {
					if let jsonData = data {
						do {
							let jsonDecoder = JSONDecoder()
							let egvResponse = try jsonDecoder.decode(DexcomAPIEGVResponse.self, from: jsonData)
							print(egvResponse)
						} catch {
							print("What happened: \(error)")
							// bad url makes error response like:
							// {\"path\":\"/egvs\",\"message\":\"Bad Request\",\"errors\":{\"message\":\"Invalid startDate and/or endDate format. Please provide in format: YYYY-MM-DDThh:mm:ss\",\"errors\":null}}
							// model and handle
						}
					}
				}
			})
			
			task.resume()
		} else {
			// re-start auth flow
		}
		
	}
	
}

// ??? is this an okay subclassing? just want some way to encapsulate this particular format
class DexcomAPIDateFormatter: DateFormatter {
	override init() {
		
		super.init()
		
		self.locale = Locale(identifier: "en_US_POSIX")// should be derived from local system, right?
		self.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
		self.timeZone = TimeZone(secondsFromGMT: 0)
	}
	
	required init?(coder aDecoder: NSCoder) {
		super.init(coder: aDecoder)
	}
}

struct DexcomAPIError: Codable {
	let path: String
	let message: String
	let errors: [String: String]
}

struct DexcomAPIEGVResponse: Codable {
	let unit: String
	let rateUnit: String
	let egvs: [DexcomAPIEGV]
}

struct DexcomAPIEGV: Codable {
	let systemTime: Date
//	let displayTime: Date// for now let's just throw this out and work from the GMT timestamp
	let value: Int
	let status: String?
	let trend: DexcomEGVTrend
	let trendRate: Double
}

enum DexcomEGVTrend: String, Codable {
	case doubleUp, singleUp, fortyFiveUp, flat, fortyFiveDown, singleDown, doubleDown, none, notComputable, rateOutOfRange
}

struct DexcomAPITokenRequest: Codable {
	let client_id: String
	let client_secret: String
	let code: String
	let grant_type: String = "authorization_code"
	let redirect_uri: String
}

struct DexcomAPITokenResponse: Codable {
	let access_token: String
	let expires_in: Int
	let token_type: String
	let refresh_token: String
}

struct DexcomAPICredentials {
	let access_token: String
	let refresh_token: String
}

struct DexcomAPILoginURL: OAuth2LoginURL {
	
}
