//
//  BGEntryModel.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-11-25.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa
import DateToolsSwift

struct BGEntry {
	
	let id: Int
	let gv: Int
	let gvMeasurement: Measurement<UnitConcentrationMass>
	let time: Date
	let trend: BGTrend
	
	init (with egv: DexcomAPIEGV) {
		self.id = Int(egv.systemTime.timeIntervalSince1970)// *should* be reliably unique at 5 minutes apart but maybe do better?
		self.gv = egv.value
		self.gvMeasurement = Measurement(value: Double(self.gv), unit: UnitConcentrationMass.milligramsPerDeciliter)
		self.time = egv.systemTime
		self.trend = BGTrend(rawValue: egv.trend.rawValue)!
	}
	
	init (with nsEntry: NightscoutEntry) {
		self.id = Int(nsEntry.date)
		self.gv = nsEntry.sgv
		self.gvMeasurement = Measurement(value: Double(self.gv), unit: UnitConcentrationMass.milligramsPerDeciliter)
		self.time = Date(timeIntervalSince1970: nsEntry.date / 1000)// @TODO check this, I thought we were having to do something wack like add some zeroes to the date?
		self.trend = nsEntry.trend.bgTrend!
	}
	
}

enum BGTrend: String {
	case doubleUp, singleUp, fortyFiveUp, flat, fortyFiveDown, singleDown, doubleDown, none, notComputable, rateOutOfRange
}



struct BGEntryViewModel {
	let entry: BGEntry
	
	func getValueAsMgDl () -> Double {
		return Double(self.entry.gv)
	}
	
	func getValueAsMmol () -> Double {
		let gramsPerMole = 0.01
		let gvAsMmolLMeasurement = self.entry.gvMeasurement.converted(to: .millimolesPerLiter(withGramsPerMole: gramsPerMole))
		return gvAsMmolLMeasurement.value
	}
	
	func getLocalizedValueStringWithUnits () -> String {
		let unitPreferences = BGUnitPreferences(rawValue: UserDefaults.standard.string(forKey: "bgUnits")!)!
		let baseString = self.getLocalizedValueStringWithoutUnits()
		return "\(baseString) \(unitPreferences.rawValue)"
	}
	
	func getLocalizedValueStringWithoutUnits () -> String {
		let unitPreferences = BGUnitPreferences(rawValue: UserDefaults.standard.string(forKey: "bgUnits")!)!
		switch unitPreferences {
		case .mgdl: return "\(Int(self.getValueAsMgDl()))"
		case .mmolL: return "\(String(format: "%.1f", self.getValueAsMmol()))"
		}
	}
	
	func asLineItemString (includeEntryAge: Bool = false) -> String {
		let directionString = self.getTrendArrow()
		let valueString = self.getLocalizedValueStringWithUnits()
		
		if (includeEntryAge) {
			let entryAgeString = self.getEntryAgeString()
			return "\(valueString) \(directionString) (\(entryAgeString))"
		} else {
			return "\(valueString) \(directionString)"
		}
		
	}
	
	func asStatusItemString (applyBackgroundColors: Bool = true) -> NSAttributedString {
		
		let statusString = "\(self.getLocalizedValueStringWithoutUnits()) \(self.getTrendArrow())"
		let formattedStatusString = NSMutableAttributedString(string: statusString)
		let wholeStringRange = NSRange(location: 0, length: statusString.count)
		
		let highThreshold = UserDefaults.standard.integer(forKey: "highThreshold")
		let lowThreshold = UserDefaults.standard.integer(forKey: "lowThreshold")
		
		formattedStatusString.addAttribute(.font, value: NSFont.menuBarFont(ofSize: NSFont.systemFontSize), range: NSRange(location: 0, length: formattedStatusString.length))
		
		if (self.isStale()) {
			formattedStatusString.addAttribute(.foregroundColor, value: BGStatusSettings.dangerColor, range: NSRange(location: 0, length: formattedStatusString.length))
			formattedStatusString.addAttribute(.font, value: NSFont.boldSystemFont(ofSize: NSFont.systemFontSize), range: NSRange(location: 0, length: formattedStatusString.length))
		}
		
		if (self.entry.gv >= highThreshold || self.entry.trend == .doubleUp) {
			if (applyBackgroundColors == true) {
				formattedStatusString.addAttribute(.foregroundColor, value: BGStatusSettings.highColor, range: wholeStringRange)
			}
			formattedStatusString.addAttribute(.font, value: NSFont.boldSystemFont(ofSize: NSFont.systemFontSize), range: NSRange(location: 0, length: formattedStatusString.length))
		} else if (self.entry.gv <= lowThreshold || self.entry.trend == .doubleDown) {
			if (applyBackgroundColors == true) {
				formattedStatusString.addAttribute(.foregroundColor, value: BGStatusSettings.lowColor, range: wholeStringRange)
			}
			formattedStatusString.addAttribute(.font, value: NSFont.boldSystemFont(ofSize: NSFont.systemFontSize), range: NSRange(location: 0, length: formattedStatusString.length))
		}
		return formattedStatusString
	}
	
	func asNotificationSubtitleString () -> String {
		let directionString = self.getTrendArrow()
		let subtitleString = "Last reading was \(self.getLocalizedValueStringWithUnits()) \(directionString)"
		return subtitleString
	}
	
	func asLatestItemAttributedString () -> NSAttributedString {
		
		let baseString = self.asLineItemString()
		let attributedString = NSMutableAttributedString(string: baseString)
		
		let entryAgeString = self.getEntryAgeString()
		let attributedEntryAgeString: NSMutableAttributedString = NSMutableAttributedString(string: " (\(entryAgeString))")
		if (self.isStale()) {
			attributedEntryAgeString.addAttribute(.foregroundColor, value: BGStatusSettings.dangerColor, range: NSRange(location: 0, length: attributedEntryAgeString.length))
		}
		
		attributedString.append(attributedEntryAgeString)
		attributedString.addAttribute(.font, value: NSFont.boldSystemFont(ofSize: 0), range: NSRange(location: 0, length: attributedString.length))
		
		return attributedString as NSAttributedString
	}
	
	func getEntryAgeString () -> String {
		let entryTimeUtc = self.entry.time
		let localTimezoneOffset = TimeInterval(NSTimeZone.local.secondsFromGMT() / 36000)
		let entryTimeLocal = entryTimeUtc.addingTimeInterval(localTimezoneOffset)
		return Date().timeAgo(since: entryTimeLocal)
	}
	
	// @TODO move this to BGEntry
	func isStale () -> Bool {
		let entryBestBeforeInMinutes = UserDefaults.standard.integer(forKey: "entryBestBeforeInMinutes")
		let entryTimeUtc = self.entry.time
		let lastEntryMinutesAgo = entryTimeUtc.minutesAgo
		
		return (lastEntryMinutesAgo >= entryBestBeforeInMinutes)
	}
	
	func getTrendArrow () -> String {
		switch self.entry.trend {
		case .flat:
			return "→"
		case .fortyFiveDown:
			return "↘"
		case .fortyFiveUp:
			return "↗"
		case .singleUp:
			return "↑"
		case .doubleUp:
			return "↑↑"
		case .singleDown:
			return "↓"
		case .doubleDown:
			return "↓↓"
		case .notComputable, .rateOutOfRange, .none:
			return ""
		}
	}
}
