//
//  BGMgdlToMmolLValueTransformer.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-09-04.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa

@objc(BGMgdlToMmolLValueTransformer) class BGMgdlToMmolLValueTransformer: ValueTransformer {
	override func transformedValue(_ value: Any?) -> Any? {
	
		
		if let incomingValueAsString = value as? String,
			let incomingValueAsDouble = Double(incomingValueAsString) {
			
			let mmolLValue = incomingValueAsDouble / 18;
			return String(format: "%.1f", mmolLValue)
		}
		
		if let incomingValueAsDouble = value as? Double {
			let mmolLValue = incomingValueAsDouble / 18;
			return String(format: "%.1f", mmolLValue)
		}
		
		return ""
		
	}
}
