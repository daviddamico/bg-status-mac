//
//  BGMenuCoordinator.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-09-03.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa
import DateToolsSwift

class BGMenuCoordinator: NSObject {
	
	let menu: NSMenu
	let olderEntriesLength = 9
	var updateLatestItemTimer: Timer?
	
	var latestEntry: BGEntry?
	var latestEntryItem: NSMenuItem
	var olderEntryItems: [NSMenuItem]
	let quitItem: NSMenuItem
	let settingsItem: NSMenuItem
	
	init (with menu: NSMenu) {
		self.menu = menu
		
		let appDelegate = NSApplication.shared.delegate as! AppDelegate
		self.quitItem = NSMenuItem(title: "Quit", action: #selector(appDelegate.quitApplication), keyEquivalent: "")
		quitItem.target = appDelegate
		self.settingsItem = NSMenuItem(title: "Settings", action: nil, keyEquivalent: "")
		settingsItem.target = appDelegate
		
		self.latestEntryItem = NSMenuItem()
		self.olderEntryItems = []
		
		super.init()
		
		self.latestEntryItem.title = "LATEST ENTRY"
		self.latestEntryItem.action = #selector(self.didClickLatestEntryItem)
		self.latestEntryItem.target = self
		
		for n in 0...(olderEntriesLength - 1) {
			self.olderEntryItems.append(NSMenuItem(title: "OLDER ENTRY \(n + 1)", action: nil, keyEquivalent: ""))
		}
		
		self.menu.addItem(self.latestEntryItem)
		self.menu.addItem(.separator())
		for entryItem in olderEntryItems {
			self.menu.addItem(entryItem)
		}
		
		self.menu.addItem(.separator())
		self.menu.addItem(self.settingsItem)
		self.menu.addItem(self.quitItem)
		
	}
	
	func updateEntryItems (with latestEntries: [BGEntry]) {
		
		self.updateLatestItemTimer = nil
		self.latestEntry = nil
		
		// pop first item off array
		let latestEntry = latestEntries[0]
		self.latestEntry = latestEntry
		let latestEntryVM = BGEntryViewModel(entry: self.latestEntry!)
		
		
		self.latestEntryItem.attributedTitle = latestEntryVM.asLatestItemAttributedString()
		
		// runs updateLatestEntryItem every second so that time-since stays up to date
		DispatchQueue.main.async {
			self.updateLatestItemTimer = Timer.scheduledTimer(timeInterval: 1.0, target: self, selector: #selector(self.updateLatestEntryItem), userInfo: nil, repeats: true)
		}
		
		var olderEntries = latestEntries
		olderEntries.removeFirst()
		
		var staleSeriesFlag = false
		for (offset, element) in self.olderEntryItems.enumerated() {
			
			let olderEntry = olderEntries[offset]
			let olderEntryVM = BGEntryViewModel(entry: olderEntry)
			let bestBeforeInMinutes = UserDefaults.standard.integer(forKey: "entryBestBeforeInMinutes")

			if (offset == 0 && olderEntryVM.isStale()) {
				element.title = olderEntryVM.asLineItemString(includeEntryAge: true)
			} else if ((offset > 0 && olderEntry.time.minutesEarlier(than: olderEntries[offset - 1].time) > bestBeforeInMinutes)
						|| staleSeriesFlag == true) {
				staleSeriesFlag = true
				element.title = olderEntryVM.asLineItemString(includeEntryAge: true)
			} else {
				element.title = olderEntryVM.asLineItemString()
			}

		}
	}
	
	@objc func updateLatestEntryItem () {
		if let latestEntry = self.latestEntry {
			let latestEntryVM = BGEntryViewModel(entry: latestEntry)
			DispatchQueue.main.async {
				self.latestEntryItem.attributedTitle = latestEntryVM.asLatestItemAttributedString()
			}
		}
	}
	
	@objc func didClickLatestEntryItem () {
		if let nightscoutURL = URL(string: UserDefaults.standard.string(forKey: "nightscoutInstanceURL")!) {
			NSWorkspace.shared.open(nightscoutURL)
		}
	}
	
}
