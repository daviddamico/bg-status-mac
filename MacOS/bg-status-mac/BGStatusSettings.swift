//
//  BGStatusSettings.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-09-03.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Foundation
import Cocoa


struct BGStatusSettings {
	
	let nightscoutInstanceURL: String
	let lowThreshold: Double
	let urgentLowThreshold: Double
	let highThreshold: Double
	let entryBestBeforeInMinutes: Int
	
	static let highColor = NSColor(red: 179.0/255.0, green: 163.0/255.0, blue: 0/255.0, alpha: 1.0)
	static let lowColor = NSColor(red: 175.0/255.0, green: 14.0/255.0, blue: 14.0/255.0, alpha: 1.0)
	static let dangerColor = NSColor(red: 175.0/255.0, green: 14.0/255.0, blue: 14.0/255.0, alpha: 1.0)
	
	
}
