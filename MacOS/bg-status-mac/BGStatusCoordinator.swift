//
//  BGStatusCoordinator.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-08-29.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa

class BGStatusCoordinator: NSObject {
	
	let nightscoutURL: URL
	let nightscoutAPI: NightscoutAPIClient

	var lastTenEntries: [BGEntry] = []
	var updateTimer: Timer?
	
	init (with nightscoutURL: URL) {
		self.nightscoutURL = nightscoutURL
		self.nightscoutAPI = NightscoutAPIClient(endpointUrl: self.nightscoutURL)
		
		super.init()
	}
	
	func startUpdating () {
		self.requestUpdate()// first update
		
		// then schedule future updates
		// updates every minute for now
		self.updateTimer = Timer.scheduledTimer(timeInterval: 60.0, target: self, selector: #selector(self.requestUpdate), userInfo: nil, repeats: true)
	}
	
	func stopUpdating () {
		self.updateTimer = nil
	}
	
	@objc func requestUpdate() {
		let appDelegate = NSApplication.shared.delegate as! AppDelegate
		
		self.nightscoutAPI.getLatestEntries(completionHandler: { (nsResponse: NightscoutAPIEntriesResponse?, error: Error?) -> Void in
			
			// @TODO and now need to go from NightscoutEntry to BGEntry
			if (error != nil) {
				// return? throw?
			}
			
			if let response = nsResponse {
				
				let entries = response.entries
				let entriesDateOrder = entries.sorted(by: { $0.date > $1.date })
				
				let newEntries: [BGEntry] = entriesDateOrder.map { BGEntry(with: $0) }
				
				let newEntryIds = newEntries.map { $0.id }
				let existingEntryIds = self.lastTenEntries.map { $0.id }
				
				if (newEntryIds != existingEntryIds) {
					self.lastTenEntries = newEntries
					self.updateStatusItem(with: self.lastTenEntries[0])
					
					if let menuCoordinator = appDelegate.menuCoordinator {
						menuCoordinator.updateEntryItems(with: self.lastTenEntries)
					}
					
					self.dispatchNotifications(with: self.lastTenEntries[0])
				}
			}
		})
	}
	
	func dispatchNotifications (with latestEntry: BGEntry) -> Void {
		
		let highThreshold = UserDefaults.standard.double(forKey: "highThreshold")
		let lowThreshold = UserDefaults.standard.double(forKey: "lowThreshold")
		let urgentLowThreshold = UserDefaults.standard.double(forKey: "urgentLowThreshold")
		
		let latestEntryVM = BGEntryViewModel(entry: latestEntry)
		
		if (latestEntryVM.getValueAsMgDl() >= highThreshold
			|| latestEntryVM.getValueAsMgDl() <= urgentLowThreshold
			|| latestEntryVM.getValueAsMgDl() <= lowThreshold
			|| latestEntry.trend == .doubleUp
			|| latestEntry.trend == .doubleDown) {
			
			let notification = NSUserNotification()
			notification.hasActionButton = false
			
			if (latestEntryVM.getValueAsMgDl() >= highThreshold) {
				notification.title = "High Reading"
			} else if (latestEntryVM.getValueAsMgDl() <= urgentLowThreshold) {
				notification.title = "Urgent Low!"
				notification.soundName = NSUserNotificationDefaultSoundName
			} else if (latestEntryVM.getValueAsMgDl() <= lowThreshold) {
				notification.title = "Low Reading"
			} else if (latestEntry.trend == .doubleUp) {
				notification.title = "Rise Rate Alert"
			} else if (latestEntry.trend == .doubleDown) {
				notification.title = "Fall Rate Alert"
			}
			
			notification.subtitle = latestEntryVM.asNotificationSubtitleString()
			NSUserNotificationCenter.default.removeAllDeliveredNotifications()
			NSUserNotificationCenter.default.deliver(notification)
		}
	}
	
	func updateStatusItem (with latestEntry: BGEntry) {
		let appDelegate = NSApplication.shared.delegate as! AppDelegate
		let statusItem = appDelegate.statusItem
		
		let latestEntryVM = BGEntryViewModel(entry: latestEntry)
		
		DispatchQueue.main.async {
			statusItem.button?.attributedTitle = latestEntryVM.asStatusItemString()
			statusItem.button?.attributedAlternateTitle = latestEntryVM.asStatusItemString(applyBackgroundColors: false)
		}
		
	}
}
