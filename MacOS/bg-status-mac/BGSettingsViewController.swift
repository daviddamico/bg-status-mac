//
//  BGSettingsViewController.swift
//  bg-status-mac
//
//  Created by David D'Amico on 2017-09-03.
//  Copyright © 2017 David D'Amico. All rights reserved.
//

import Cocoa

class BGSettingsViewController: NSViewController {

	@IBOutlet weak var nightscoutInstanceURLField: NSTextField!
	@IBOutlet weak var lowThresholdField: NSTextField!
	@IBOutlet weak var urgentLowThresholdField: NSTextField!
	@IBOutlet weak var highThresholdField: NSTextField!
	@IBOutlet weak var openAtLoginCheckbox: NSButton!
	
	override func viewDidLoad() {
		super.viewDidLoad()
	}
	
}
